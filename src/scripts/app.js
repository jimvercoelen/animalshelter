$(document).ready(function() {
  var $pageTitle = $('.navbar-brand');
  var $navItem = $('.nav-item');
  var $inputText = $("input[type=text]");
  var $submitButton = $('.submit');
  var $name = $('#name');
  var $age = $('#age');
  var $regNr = $('#regNr');
  var $reservedCheckbox = $('#cb1');
  var $reservedLabel = $('#reserved');
  var $animalSpecies = $('#species');
  var $animalTableBody = $('#table-body');

  var isReserved = false;
  var animals = JSON.parse(localStorage.getItem('animals'));

  initialize();

  /**
   * Constructor
   */
  function initialize () {
    checkLocalStorage();
    initializeDeleteEvents();
    loadAllAnimals();
  }


  function checkLocalStorage() {
    if(!Array.isArray(animals)) {
      animals = [];
      loadDefaultAnimals();
      initializeDeleteEvents();
    }
  }


  $navItem.on('click', function () { scrollToElement(this.hash); });
  $pageTitle.on('click', function () { scrollToElement(this.hash); });

  function scrollToElement (hash) {
    event.preventDefault();

    var link = hash;
    var $link = $(link);
    var offset = 0;

    if (link == "#header") {
      offset = -90;
    }

    $.smoothScroll({
      scrollTarget: $link,
      offset: offset,
      speed: 1200
    });

    window.location.href.substr(0, window.location.href.indexOf('#'));
  }


  $reservedCheckbox.on('click', function () {

    if ($reservedCheckbox.is(':checked')) {
      $reservedLabel.text("Gereserveerd");
      isReserved = true;
    } else {
      $reservedLabel.text("Niet gereserveerd");
      isReserved = false;
    }
  });


  $inputText.on('click', function () {

    switch (this.name) {
      case "name":
        $(this).attr('placeholder', 'Vul de naam in.');
        break;
      case "age":
        $(this).attr('placeholder', 'Vul de leeftijd in.');
        break;
      case "regNr":
        $(this).attr('placeholder', 'Vul een registratie nummer in.');
        break;
    }
  });


  /**
   * Register animal process:
   * [1] Form validation, each input field is required
   * [2] Add a new animal into the animals array
   * [3] reset localstorage (including the new animal)
   * [4] call LoadAnimal
   * [5] call scrollToElement (to animals table)
   */
    $submitButton.on('click', function submitAnimalProcess (e) {

      e.preventDefault();

      if (!$animalSpecies.val()) {
        $('#default').text("Een diersoort kiezen is verplicht");
      } else if (!$name.val().trim()) {
        $($name).attr('placeholder', 'De naam is verplicht.');
      } else if (!$age.val().trim()) {
        $($age).attr('placeholder', 'De leeftijd is verplicht.');
      } else if (!$regNr.val().trim()) {
        $($regNr).attr('placeholder', 'Een registratie nummer is verplicht.');
      } else {

        var animal = {
          species: $animalSpecies.val(),
          name: $name.val(),
          age: $age.val(),
          regNr: $regNr.val(),
          isReserved: (isReserved ? "ja" : "nee")
        };

        animals.push(animal);
        localStorage.setItem('animals', JSON.stringify(animals));

        addAnimal(animal);
        scrollToElement('#animals-in-shelter');
        initializeDeleteEvents();
      }
    });



  function initializeDeleteEvents () {
    var onDelete = function () {
      var cell = $(this).closest('td');
      var row = cell.closest('tr');
      var rowIndex = row[0].rowIndex;
      var animalsIndex = rowIndex - 1;

      $("tr").eq(rowIndex).remove();
      animals.splice(animalsIndex, 1);
      localStorage.setItem('animals', JSON.stringify(animals));
    };
    $('.icon-delete').on('click', onDelete);
  }


  function addAnimal (animal) {
    var html =
      '<tr>' +
        '<td>' + animal.species + '</td>' +
        '<td>' + animal.name + '</td>' +
        '<td>' + animal.age + '</td>' +
        '<td>' + animal.regNr + '</td>' +
        '<td>' + animal.isReserved + '</td>' +
        '<td><div class="icon-delete"></div></td>' +
      '</tr>';

    $animalTableBody.append(html);
  };


  /**
   * Load all animals into the table of animals
   */
  function loadAllAnimals () {

    //localStorage.clear();
    var html =  '';

    animals.forEach(function (animal) {
      html +=
        '<tr>' +
          '<td>' + animal.species + '</td>' +
          '<td>' + animal.name + '</td>' +
          '<td>' + animal.age + '</td>' +
          '<td>' + animal.regNr + '</td>' +
          '<td>' + animal.isReserved + '</td>' +
          '<td><div class="icon-delete"></div></td>' +
        '</tr>';
    });

    $animalTableBody.append(html);
    initializeDeleteEvents();
  }

  function loadDefaultAnimals () {
    var animal1 = {
      species: "Hond",
      name: "Blaffer",
      age: "1 jaar",
      regNr: "12093",
      isReserved: "ja"
    };

    var animal2 = {
      species: "Kat",
      name: "Roetje",
      age: "2 maanden",
      regNr: "12094",
      isReserved: "nee"
    };

    var animal3 = {
      species: "Hamsterachtige",
      name: "Dropje",
      age: "2,5 jaar",
      regNr: "12095",
      isReserved: "ja"
    };

    var animal4 = {
      species: "Vogel",
      name: "Karel",
      age: "4 jaar",
      regNr: "12096",
      isReserved: "nee"
    };

    animals.push(animal1);
    animals.push(animal2);
    animals.push(animal3);
    animals.push(animal4);
  }
});


