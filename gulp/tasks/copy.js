var gulp = require('gulp');

gulp.task('copy:favicons', function () {
  return gulp
    .src('src/favicon*')
    .pipe(gulp.dest('target/dist'));
});

gulp.task('copy:fonts', function () {
  return gulp
    .src('.tmp/fonts/*')
    .pipe(gulp.dest('target/dist/fonts'));
});

gulp.task('copy:sprites', function () {
  return gulp
    .src('.tmp/images/*')
    .pipe(gulp.dest('target/dist/images'));
});

gulp.task('copy:images', function () {
  return gulp
    .src('src/images/**/*')
    .pipe(gulp.dest('target/dist/images'));
});

gulp.task('copy:sitemap', function () {
  return gulp
    .src('src/sitemap.xml')
    .pipe(gulp.dest('target/dist'));
});

gulp.task('copy:robots', function () {
  return gulp
    .src('src/robots.txt')
    .pipe(gulp.dest('target/dist'));
});

gulp.task('copy', ['copy:fonts', 'copy:favicons', 'copy:sprites', 'copy:images', 'copy:sitemap', 'copy:robots']);
