var gulp = require('gulp');
var sass = require('gulp-sass');
var minifyCss = require('gulp-minify-css');
var filter = require('gulp-filter');
var rev = require('gulp-rev');
var assets = require('gulp-assets');

var browserSync = require('./browser-sync');
var common = require('../common');

gulp.task('styles', function () {
  return common.getCombinerPipe('styles', [
    gulp.src([
      'src/styles/main.scss'
    ]),
    sass(),
    gulp.dest('.tmp/styles'),
    browserSync.stream()
  ]);
});

gulp.task('styles:dist', function () {
  var cssFilter = filter('*.css', {
    restore: true
  });

  return gulp
    .src('src/styles/main.scss')
    .pipe(sass())
    .pipe(minifyCss())
    .pipe(cssFilter)
    .pipe(rev())
    .pipe(cssFilter.restore)
    .pipe(gulp.dest('target/dist/styles'))
    .pipe(rev.manifest())
    .pipe(gulp.dest('target/dist/styles'));
});
