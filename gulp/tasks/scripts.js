var gulp = require('gulp');
var concat = require('gulp-concat');
var assets = require('gulp-assets');
var uglify = require('gulp-uglify');
var rev = require('gulp-rev');
var filter = require('gulp-filter');

gulp.task('scripts', function () {
  return gulp
    .src('src/index.html')
    .pipe(assets.js())
    .pipe(concat('app.js'))
    .pipe(gulp.dest('target/dist/scripts'));
});

gulp.task('scripts:dist', function () {
  var jsFilter = filter('app.js', {
    restore: true
  });

  return gulp
    .src('src/index.html')
    .pipe(assets.js())
    .pipe(concat('app.js'))
    .pipe(uglify())
    .pipe(jsFilter)
    .pipe(rev())
    .pipe(jsFilter.restore)
    .pipe(gulp.dest('target/dist/scripts'))
    .pipe(rev.manifest())
    .pipe(gulp.dest('target/dist/scripts'));
});
