var gulp = require('gulp');
var htmlReplace = require('gulp-html-replace');
var htmlMinify = require('gulp-minify-html');

// setting an option to true means, "do not ..."
// default are all setting false
var htmlMinifyOptions = {
  empty: false,
  cdate: false,
  comments: false,
  conditionals: false,
  spare: false,
  quotes: false,
  loose: false
};

gulp.task('html:dist', ['styles:dist', 'scripts:dist'], function () {
  var stylesManifest = require('../../target/dist/styles/rev-manifest.json');
  var scriptsManifest = require('../../target/dist/scripts/rev-manifest.json');

  return gulp
    .src([
      'src/index.html',
      'src/404.html'
    ])
    .pipe(htmlReplace({
      css: 'styles/' + stylesManifest['main.css'],
      js: 'scripts/' + scriptsManifest['app.js']
    }))
    .pipe(htmlMinify(htmlMinifyOptions))
    .pipe(gulp.dest('target/dist'));
});
